package org.geoframe.ocn;

import java.io.File;

import org.geoframe.io.*;
import org.geoframe.util.CreateFolder;
import org.geoframe.util.InfoFolder;

/**
 * Calculates  and stores the total contributing areas of a river basin.
 * @author Riccardo Rigon 2013
 * @version 1/16
 * @see Raster
 * @see IRiverBasin
 * 
 */
public class TCA extends Raster implements IRiverBasin {
/**
 * NO_VALUE is set to -1 for TCA
 * tca is a "matrix" of long instead than of int	
 */
	final static long NO_VALUE=-1;	
	long[][] tca=null;
	public double energyExpenditure;
//Constructor
/**
 * sets and initializes a tca object.	
 * @param rows
 * @param cols
 */
	public TCA(int rows,int cols){
		this.rows=rows;
		this.cols=cols;
		tca=new long[rows][cols];
		//Feed the object
		for(int j=0;j<tca[0].length;j++){
			tca[0][j]=NO_VALUE;
		}
		for(int j=0;j<tca[0].length;j++){
			tca[tca.length-1][j]=NO_VALUE;
		}
		for(int i=0;i<tca.length;i++){
			tca[i][0]=NO_VALUE;
		}
		for(int i=0;i<tca.length;i++){
			tca[i][tca[0].length-1]=NO_VALUE;
		}
		//The core of the basin (Useless indeed since that the matrix has already been initialised to 0
		for(int i=1;i<tca.length-1;i++){
			for(int j=1;j<tca[0].length-1;j++){
				tca[i][j]=0;
			}
		}


	}

	public static void main(String[] args) {
		System.out.println("This is TCA main()");
		
		System.out.println("Please enter the name of the river:");
		String nm=TextIO.getln();
		System.out.println("Please enter the number of rows:");
		int rws=TextIO.getInt();
		System.out.println("Please enters the number of columns:");
		int cls=TextIO.getInt();
		TCA tca=new TCA(rws,cls);
		tca.basinName=nm;
		tca.rows=rws;
		tca.cols=cls;
		
		tca.print();
        System.out.println("This ends computation");
	}
/**
 * prints the data in a tca object
 */
	@Override
	public void print() {
		
		for(int i=0;i<this.tca.length;i++){
			for(int j=0;j<this.tca[0].length;j++){
				System.out.print(this.tca[i][j]);
				System.out.print("\t");
			}
			System.out.print("\n");
		}
	}
	/**
	 * writes an Esri ascii file containing the values of the total contributing area.
	 * 
	 */
	@Override
	public void asciiRasterWriter() {
		
		String outputFileName=CreateFolder.getFileName();
		//Rename the old file name if it exists;
		File outputFile=new File(outputFileName);
		if(outputFile.exists()){
			File oldFile=new File(outputFileName+".old");
			outputFile.renameTo(oldFile);
		}
		//Write the header: This is not so general, however, should take
		//variable inputs
	    TextIO.writeFile(outputFileName);
	    TextIO.putf("NCOLS %d\n", cols);
	    TextIO.putf("NROWS %d\n",rows);
	    TextIO.putf("XLLCORNER %d\n", 0);
	    TextIO.putf("YLLCORNER %d\n", 0);
	    TextIO.putf("CELLSIZE %d\n", 1000);
	    TextIO.putf("NODATA_VALUE %d\n", NO_VALUE);
	    for(int i=0;i<rows;i++){
	    	for(int j=0; j<cols;j++){
			 TextIO.putf("%d ",this.tca[i][j]);
	    	}
	    	TextIO.putln();
	    }
	}

/**
 * Writes a file with the same name that flow has
 * @param info
 */

public void asciiRasterWriter(InfoFolder info) {
	
	String outputFileName=info.workingPathName+"_tca.asc";
	//Write the header: This is not so general, however, should take
	//variable inputs
    TextIO.writeFile(outputFileName);
    TextIO.putf("NCOLS %d\n", cols);
    TextIO.putf("NROWS %d\n",rows);
    TextIO.putf("XLLCORNER %d\n", 0);
    TextIO.putf("YLLCORNER %d\n", 0);
    TextIO.putf("CELLSIZE %d\n", 1000);
    TextIO.putf("NODATA_VALUE %d\n", NO_VALUE);
    for(int i=0;i<rows;i++){
    	for(int j=0; j<cols;j++){
		 TextIO.putf("%d ",this.tca[i][j]);
    	}
    	TextIO.putln();
    }
}



}
